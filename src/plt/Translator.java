package plt;

public class Translator {
	
	public static final String NIL = "nil";
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}


	public String getPhrase() {
		return phrase;
	}


	public String translate() {
		if(!(phrase.isEmpty())) { 
			String result = "";
			String punctuation = "";
			if(phrase.contains(" ")) {
				String[] words = phrase.split(" ");
				for(int i= 0; i<words.length; i++) {
					if(words[i].startsWith(".")){
						result += ".";
						words[i] = words[i].replaceAll("\\.","");
					}
					if(words[i].startsWith(",")){
						result += ",";
						words[i] = words[i].replace(",","");
					} 
					if(words[i].startsWith(";")){
						result += ";";
						words[i] = words[i].replace(";","");
					}
					if(words[i].startsWith(":")){
						result += ":";
						words[i] = words[i].replace(":","");
					}
					if(words[i].startsWith("?")){
						result += "?";
						words[i] = words[i].replaceAll("\\?","");
					} 
					if(words[i].startsWith("!")){
						result += "!";
						words[i] = words[i].replace("!","");
					} 
					if(words[i].startsWith("'")){
						result += "'";
						words[i] = words[i].replace("'","");
					}
					if(words[i].startsWith("(")){
						result += "(";
						words[i] = words[i].replaceAll("\\(","");
					} 
					if(words[i].startsWith(")")){
						result += ")";
						words[i] = words[i].replaceAll("\\)","");
					}
					
					if(words[i].endsWith(".")){
						punctuation = ".";
						words[i] = words[i].replaceAll("\\.","");
					}
					if(words[i].endsWith(",")){
						punctuation = ",";
						words[i] = words[i].replace(",","");
					} 
					if(words[i].endsWith(";")){
						punctuation = ";";
						words[i] = words[i].replace(";","");
					}
					if(words[i].endsWith(":")){
						punctuation = ":";
						words[i] = words[i].replace(":","");
					}
					if(words[i].endsWith("?")){
						punctuation = "?";
						words[i] = words[i].replaceAll("\\?","");
					} 
					if(words[i].endsWith("!")){
						punctuation = "!";
						words[i] = words[i].replace("!","");
					} 
					if(words[i].endsWith("'")){
						punctuation = "'";
						words[i] = words[i].replace("'","");
					}
					if(words[i].endsWith("(")){
						punctuation = "(";
						words[i] = words[i].replaceAll("\\(","");
					} 
					if(words[i].endsWith(")")){
						punctuation = ")";
						words[i] = words[i].replaceAll("\\)","");
					}
						
					if(startsWithVowel(words[i])){
						if(words[i].endsWith("y")) {
								result += words[i] + "nay ";
							}
						else if(endsWithVowel(words[i])) {
							result += words[i] + "yay ";
						}	
						else if(!endsWithVowel(words[i])) {
						   	result += words[i] + "ay ";
					   	}
					}
					else if(!startsWithVowel(words[i])){
							String newPhrase = "";
							if(!(secondIsVowel(words[i]))){
								newPhrase = newPhrase + words[i].substring(2) + words[i].substring(0,2) + "ay ";
								result += newPhrase;
							}
								newPhrase = newPhrase + words[i].substring(1) + words[i].substring(0,1) + "ay ";
								result += newPhrase;
						}
					if(!"".equals(punctuation)) {
						result = result.substring(0,result.length()-1) + punctuation;
						result += " ";
					}
				}
				return result.stripTrailing();
		}
		else if(phrase.contains("-")) {
				String[] words = phrase.split("-");
				for(int i= 0; i<words.length; i++) {
					if(words[i].startsWith(".")){
						result += ".";
						words[i] = words[i].replaceAll("\\.","");
					}
					if(words[i].startsWith(",")){
						result += ",";
						words[i] = words[i].replace(",","");
					} 
					if(words[i].startsWith(";")){
						result += ";";
						words[i] = words[i].replace(";","");
					}
					if(words[i].startsWith(":")){
						result += ":";
						words[i] = words[i].replace(":","");
					}
					if(words[i].startsWith("?")){
						result += "?";
						words[i] = words[i].replaceAll("\\?","");
					} 
					if(words[i].startsWith("!")){
						result += "!";
						words[i] = words[i].replace("!","");
					} 
					if(words[i].startsWith("'")){
						result += "'";
						words[i] = words[i].replace("'","");
					}
					if(words[i].startsWith("(")){
						result += "(";
						words[i] = words[i].replaceAll("\\(","");
					} 
					if(words[i].startsWith(")")){
						result += ")";
						words[i] = words[i].replaceAll("\\)","");
					}
					if(words[i].endsWith(".")){
						punctuation = ".";
						words[i] = words[i].replaceAll("\\.","");
					}
					if(words[i].endsWith(",")){
						punctuation = ",";
						words[i] = words[i].replace(",","");
					} 
					if(words[i].endsWith(";")){
						punctuation = ";";
						words[i] = words[i].replace(";","");
					}
					if(words[i].endsWith(":")){
						punctuation = ":";
						words[i] = words[i].replace(":","");
					}
					if(words[i].endsWith("?")){
						punctuation = "?";
						words[i] = words[i].replaceAll("\\?","");
					} 
					if(words[i].endsWith("!")){
						punctuation = "!";
						words[i] = words[i].replace("!","");
					} 
					if(words[i].endsWith("'")){
						punctuation = "'";
						words[i] = words[i].replace("'","");
					}
					if(words[i].endsWith("(")){
						punctuation = "(";
						words[i] = words[i].replaceAll("\\(","");
					} 
					if(words[i].endsWith(")")){
						punctuation = ")";
						words[i] = words[i].replaceAll("\\)","");
					}
					if(startsWithVowel(words[i])){
						if(words[i].endsWith("y")) {
								result += words[i] + "nay-";
							}
						else if(endsWithVowel(words[i])) {
							result += words[i] + "yay-";
						}	
						else if(!endsWithVowel(words[i])) {
							result += words[i] + "ay-";
						   }
						}
						else if(!startsWithVowel(words[i])){
								String newPhrase = "";
								if(!(secondIsVowel(words[i]))){
									newPhrase += words[i].substring(2) + words[i].substring(0,2) + "ay-";
									result += newPhrase;
								}
									newPhrase += words[i].substring(1) + words[i].substring(0,1) + "ay-";
									result += newPhrase;
							}
					if(!"".equals(punctuation)) {
						result = result.substring(0,result.length()-1) + punctuation;
						result += "-";
					}
				}
				result = result.stripTrailing();
				return result.substring(0,result.length()-1);
		}
			if(phrase.startsWith(".")){
				result += ".";
				phrase = phrase.replaceAll("\\.","");
			}
			if(phrase.startsWith(",")){
				result += ",";
				phrase = phrase.replace(",","");
			} 
			if(phrase.startsWith(";")){
				result += ";";
				phrase = phrase.replace(";","");
			}
			if(phrase.startsWith(":")){
				result += ":";
				phrase = phrase.replace(":","");
			}
			if(phrase.startsWith("?")){
				result += "?";
				phrase = phrase.replaceAll("\\?","");
			} 
			if(phrase.startsWith("!")){
				result += "!";
				phrase = phrase.replace("!","");
			} 
			if(phrase.startsWith("'")){
				result += "'";
				phrase = phrase.replace("'","");
			}
			if(phrase.startsWith("(")){
				result += "(";
				phrase = phrase.replaceAll("\\(","");
			} 
			if(phrase.startsWith(")")){
				result += ")";
				phrase = phrase.replaceAll("\\)","");
			}
			if(phrase.endsWith(".")){
				punctuation = ".";
				phrase = phrase.replaceAll("\\.","");
			}
			if(phrase.endsWith(",")){
				punctuation = ",";
				phrase = phrase.replace(",","");
			} 
			if(phrase.endsWith(";")){
				punctuation = ";";
				phrase = phrase.replace(";","");
			}
			if(phrase.endsWith(":")){
				punctuation = ":";
				phrase = phrase.replace(":","");
			}
			if(phrase.endsWith("?")){
				punctuation = "?";
				phrase = phrase.replaceAll("\\?","");
			} 
			if(phrase.endsWith("!")){
				punctuation = "!";
				phrase = phrase.replace("!","");
			} 
			if(phrase.endsWith("'")){
				punctuation = "'";
				phrase = phrase.replace("'","");
			}
			if(phrase.endsWith("(")){
				punctuation = "(";
				phrase = phrase.replaceAll("\\(","");
			} 
			if(phrase.endsWith(")")){
				punctuation = ")";
				phrase = phrase.replaceAll("\\)","");
			}
		else if(startsWithVowel(phrase)){
			if(phrase.endsWith("y")) {
					result += phrase + "nay";
				}
				else if(endsWithVowel(phrase)) {
					result += phrase + "yay";
				}	
				else if(!endsWithVowel(phrase)) {
					result += phrase + "ay";
				}
		}
		else if(!startsWithVowel(phrase)){
			String newPhrase = "";
			if(!(secondIsVowel(phrase))){
					newPhrase = phrase.substring(2) + phrase.substring(0,2) + "ay";
					result += newPhrase;
				} else {
					newPhrase = phrase.substring(1) + phrase.substring(0,1) + "ay";
					result += newPhrase;
				}
		}
			if(Character.isUpperCase((phrase.charAt(0)))) {
				result = result.toLowerCase();
				result = result.substring(0,1).toUpperCase() + result.substring(1);
			}
			if(!"".equals(punctuation)) {
				result += punctuation;
			}
			return result;	
		}
			return NIL;
	}


	private boolean startsWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || phrase.startsWith("u");
	}
	
	private boolean endsWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u");
	}
	
	private boolean secondIsVowel(String word) {
		String index = word.substring(0,2);
		return index.endsWith("a") || index.endsWith("e") || index.endsWith("i") || index.endsWith("o") || index.endsWith("u");
	}
}
